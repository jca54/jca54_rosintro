#Script based on MoveGroupPythonInterfaceTutorial from ros-planning.github.io

# Python 2/3 compatibility imports
# Import needed stuff
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# Initialize MoveIt Commander and a Rospy node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

#Instantiate Robot Commander Object
robot = moveit_commander.RobotCommander()

#Instantiate Planning Scene Interface Object
scene = moveit_commander.PlanningSceneInterface()

#Instantiate Move Group Commander Object
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

#Create Display Trajectory ROS Publisher
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

#Get joint values of the group and change the values
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 8
joint_goal[2] = tau / 4
joint_goal[3] = -tau / 2.75
joint_goal[4] = -tau / 4
joint_goal[5] = 0 
move_group.go(joint_goal, wait=True)
move_group.stop()

#Create Positon 2
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 6
joint_goal[2] = tau / 3
joint_goal[3] = -tau / 2.75
joint_goal[4] = -tau / 4
joint_goal[5] = 0 
move_group.go(joint_goal, wait=True)
move_group.stop()

#Create Positon 3
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = tau / 50
joint_goal[1] = -tau / 6
joint_goal[2] = tau / 3
joint_goal[3] = -tau / 2.75
joint_goal[4] = -tau / 4
joint_goal[5] = 0 
move_group.go(joint_goal, wait=True)
move_group.stop()

#Create Positon 4
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = tau / 50
joint_goal[1] = -tau / 7
joint_goal[2] = tau / 3.25
joint_goal[3] = -tau / 2.75
joint_goal[4] = -tau / 4
joint_goal[5] = 0 
move_group.go(joint_goal, wait=True)
move_group.stop()
