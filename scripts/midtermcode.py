# Code to write "JCA" using UR5 in Gazebo
# Script based on MoveGroupPythonInterfaceTutorial from ros-planning.github.io

# Python compatibility imports, imports needed packages and functions
from __future__ import print_function
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
# Import math values
try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# Initialize MoveIt Commander and a Rospy node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
# Instantiate Robot Commander Object
robot = moveit_commander.RobotCommander()
# Instantiate Planning Scene Interface Object
scene = moveit_commander.PlanningSceneInterface()
# Instantiate Move Group Commander Object
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
# Create Display Trajectory ROS Publisher
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# Create function to move robot to a pose based on joint values
# Takes 6 joint angles as input, and moves UR5 joints to specified joint angles
def movejoint(j0, j1, j2, j3, j4, j5):
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = j0
    joint_goal[1] = j1
    joint_goal[2] = j2
    joint_goal[3] = j3
    joint_goal[4] = j4
    joint_goal[5] = j5
    move_group.go(joint_goal, wait=True)
    move_group.stop()
    return


# Write "J" using 4 poses
movejoint(0, -0.48869, 1, -0.523, 0, 0) #B
movejoint(0, -1.0297, 2.3736, -0.6632, 0, -0.6981) #F
movejoint(0.5934, -1.012, 2.164, -1.152, 0.5934, 0) #E
movejoint(0.4014, -0.8378, 1.745, -0.9076, 0.4014, 0) #C

# Write "C" using 4 poses
movejoint(0, -0.48869, 1, -0.523, 0, 0) #B
movejoint(0.2618, -0.3316, 0.6632, -0.3316, 0.2618, 0) #A
movejoint(0.5934, -1.012, 2.164, -1.152, 0.5934, 0) #E
movejoint(0, -1.0297, 2.3736, -0.6632, 0, -0.6981) #F

# Write "A" using 6 poses
movejoint(0, -1.0297, 2.3736, -0.6632, 0, -0.6981) #F
movejoint(0, -0.48869, 1, -0.523, 0, 0) #B
movejoint(0.2618, -0.3316, 0.6632, -0.3316, 0.2618, 0) #A
movejoint(0.5934, -1.012, 2.164, -1.152, 0.5934, 0) #E
movejoint(0.4014, -0.8378, 1.745, -0.9076, 0.4014, 0) #C
movejoint(0, -0.8727, 1.85, -0.5585, 0, -0.4363) #D
